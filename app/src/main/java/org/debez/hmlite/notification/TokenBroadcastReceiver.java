package org.debez.hmlite.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

/**
 * Created by Bervianto Leo P on 14/02/2017.
 */

public abstract class TokenBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "TokenBroadcastReceiver";

    public static final String ACTION_TOKEN = "com.google.example.ACTION_TOKEN";
    public static final String EXTRA_KEY_TOKEN = "key_token";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive:" + intent);

        if (ACTION_TOKEN.equals(intent.getAction())) {
            if (intent.getExtras() != null) {
                String token = intent.getExtras().getString(EXTRA_KEY_TOKEN);
                onNewToken(token);
            }
        }
    }

    public static IntentFilter getFilter() {
        return new IntentFilter(ACTION_TOKEN);
    }

    public abstract void onNewToken(String token);

}