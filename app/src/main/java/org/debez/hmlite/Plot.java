package org.debez.hmlite;

/**
 * Created by Silva on 2/25/2017.
 */

public class Plot {
    private int plantId;
    private int plantPhase;
    private int plotId;
    private String creatureName;
    private int creatureId;

    Plot(int plot){
        this(0,0,plot,"",0);
    }

    Plot(int plant_id, int phase, int plot_id, String c_name, int c_id){
        this.plantId = plant_id;
        this.plantPhase = phase;
        this.plotId = plot_id;
        this.creatureName = c_name;
        this.creatureId = c_id;
    }

    public int getCreatureId() {
        return creatureId;
    }

    public int getPlantId() {
        return plantId;
    }

    public int getPlantPhase() {
        return plantPhase;
    }

    public int getPlotId() {
        return plotId;
    }

    public String getCreatureName() {
        return creatureName;
    }
}