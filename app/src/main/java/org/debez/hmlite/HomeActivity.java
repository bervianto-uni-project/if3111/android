package org.debez.hmlite;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
//import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.debez.hmlite.auth.LoginActivity;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bervianto Leo P on 14/02/2017.
 */

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SensorEventListener, LocationListener {
    // Firebase
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    private FirebaseUser user_active;

    // View Content
    private View header;
    private SwipeRefreshLayout swipe_profile;

    // Lcation
    private LocationManager locationManager;
    private String provider;
    private int lat, lng;
    private String countryName;

    // Proximity
    private SensorManager mSensorManager;
    private Sensor mSensor;

    // shared preferences
    public static SharedPreferences userPrefShared;
    public static String userPref;
    public static final String user_location = "locationKey";
    public static final String user_level = "levelKey";
    public static final String user_name_key = "nameKey";
    public static final String user_stamina = "staminaKey";
    public static final String user_gold = "goldKey";
    public static final String user_exp = "expKey";

    private final String log_home = "Home";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Initialize
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // start proximity
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        // firebase auth setup
        auth = FirebaseAuth.getInstance();
        user_active = auth.getCurrentUser();
        authListener = firebaseAuth -> {
            FirebaseUser user = firebaseAuth.getCurrentUser();
            if (user == null) {
                // user auth state is changed - user is null
                // launch login activity
                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                finish();
            }
        };

        setupFloatingBar();
        setupNavigationBar();
        setHeader();

        if (user_active != null) {
            userPref = user_active.getUid();
            userPrefShared = getSharedPreferences(userPref, Context.MODE_PRIVATE);
        }

        locationUpdate();
        setupSwipe();
    }

    private void setupSwipe() {
        /*
         * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
         * performs a swipe-to-refresh gesture.
         */
        swipe_profile = findViewById(R.id.home_swipe_refresh);

        swipe_profile.setOnRefreshListener(
                () -> {
                    Log.i(log_home, "onRefresh called from SwipeRefreshLayout");

                    // This method performs the actual data-refresh operation.
                    // The method calls setRefreshing(false) when it's finished.
                    update();
                }
        );
    }

    private void setupFloatingBar() {
        // floating action button
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.subject_intent));
            sendIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(getString(R.string.extra_text_intent)));
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getString(R.string.custom_intent_name)));
        });
    }

    private void setupNavigationBar() {
        // Navigation Bar
        // set toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    private void update() {
        FragmentManager fm = getSupportFragmentManager();
        //if you added fragment via layout xml
        CharacterFragment fragment = (CharacterFragment) fm.findFragmentById(R.id.fragment2);
        if (fragment != null)
            fragment.update();
        swipe_profile.setRefreshing(false);
    }

    private void setHeader() {
        // setup navigation
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);
        if (user_active != null) {
            TextView user_name = header.findViewById(R.id.displaynameuser);
            user_name.setText(user_active.getDisplayName());
            TextView user_mail = header.findViewById(R.id.display_mail_user);
            user_mail.setText(user_active.getEmail());
        }
    }

    private void locationUpdate() {
        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        Location location = locationManager.getLastKnownLocation(provider);
        // Initialize the location fields
        if (location != null) {
            String log = "Provider " + provider + " has been selected.";
            Log.d(log_home, log);
            onLocationChanged(location);
        }

        // setup or handle input location
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null) {
            if (addresses.size() > 0) {
                countryName = addresses.get(0).getCountryName();
                if (countryName != null) {
                    SharedPreferences.Editor editor = userPrefShared.edit();
                    editor.putString(user_location, countryName);
                    editor.apply();
                } else {
                    countryName = userPrefShared.getString(user_location, null);
                }
                if (countryName.equalsIgnoreCase("indonesia")) {
                    ImageView img = (ImageView) header.findViewById(R.id.imageView);
                    img.setImageResource(R.drawable.indonesia);
                }
            }
            Log.d(log_home, "country : " + countryName);
        }
    }

    //sign out method
    private void signOut() {
        auth.signOut();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(provider, 400, 1, this);
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
        // bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        lat = (int) (location.getLatitude());
        lng = (int) (location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onProviderEnabled(String provider) {
        //Toast.makeText(this, getString(R.string.location_enabled_message), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        //Toast.makeText(this, getString(R.string.location_disabled_message), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_dice) {
            startActivity(new Intent(HomeActivity.this, DiceRollActivity.class));
        } else if (id == R.id.nav_plant_status) {
            startActivity(new Intent(HomeActivity.this, PlantActivity.class));
        } else if (id == R.id.nav_preferences) {
            // TODO Preferences Activity
        } else if (id == R.id.nav_signout) {
            signOut();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.values[0] == 0) {
            startActivity(new Intent(HomeActivity.this, DiceRollActivity.class));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}