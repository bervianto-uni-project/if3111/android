package org.debez.hmlite;

/**
 * Created by Bervianto Leo P on 06/04/2017.
 */

public class Plant {
    private int plantId;
    private int plantPhase;
    private String creatureName;
    private String plantDeathTime;
    private String plantHarvestTime;
    private String plantTillGrowth;

    public Plant(int plantId, int plant_phase, String creature_name, String plant_death_time, String plant_harvest_time, String plant_till_growth) {
        this.plantId = plantId;
        this.plantPhase = plant_phase;
        this.creatureName = creature_name;
        this.plantDeathTime = plant_death_time;
        this.plantHarvestTime = plant_harvest_time;
        this.plantTillGrowth = plant_till_growth;
    }

    public int getPlantId() {
        return plantId;
    }

    public int getPlantPhase() {
        return plantPhase;
    }

    public String getCreatureName() {
        return creatureName;
    }

    public String getPlantDeathTime() {
        return plantDeathTime;
    }

    public String getPlantHarvestTime() {
        return plantHarvestTime;
    }

    public String getPlantTillGrowth() {
        return plantTillGrowth;
    }
}
