package org.debez.hmlite.services;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Silva on 2/23/2017.
 * Class for connecting to a rest
 */

public class RestConnectService {

    /**
     * Web Service URL
     */
    private URL wsURL;
    /**
     * String Result
     */
    private String result;
    /**
     * JSON Object from result
     */
    private JSONObject jsonObject;
    /**
     * request method
     */
    private String requestMethod;
    private static final String TAG = RestConnectService.class.getSimpleName();

    /**
     * Default Constructor
     */
    public RestConnectService() {
    }

    /**
     * Constructor
     */
    public RestConnectService(URL url, String requestMethod) {
        wsURL = url;
        this.requestMethod = requestMethod;
    }

    public void startReceive() {
        try {
            URL url = wsURL;
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(requestMethod);
            InputStream in = new BufferedInputStream(conn.getInputStream());
            result = convertStreamToString(in);
            //setJsonfromString(result);
            conn.disconnect();
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException " + e.getMessage());
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    //Sending Data
    public void startSending(String data) {
        try {
            URL url;
            if (requestMethod.equals("GET")) {
                url = new URL(getWsURL().toString() + data);
            } else {
                url = wsURL;
            }
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(requestMethod);
            if (requestMethod.equals("POST")) {
                conn.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");

                conn.setRequestProperty("Content-Length", "" +
                        Integer.toString(data.getBytes().length));
                conn.setRequestProperty("Content-Language", "en-US");

                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
                wr.writeBytes(data);
                wr.flush();
                wr.close();
            }

            InputStream is = conn.getInputStream();
            result = convertStreamToString(is);
            setJsonfromString(result);
            conn.disconnect();
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }

    }

    //from http://www.androidhive.info/2012/01/android-json-parsing-tutorial/
    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public URL getWsURL() {
        return wsURL;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public String getResult() {
        return result;
    }

    public void setWsURL(URL url) {
        wsURL = url;
    }

    public void setRequestMethod(String requestmethod) {
        requestMethod = requestmethod;
    }

    private void setJsonfromString(String JSONString) {

        try {
            jsonObject = new JSONObject(JSONString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
