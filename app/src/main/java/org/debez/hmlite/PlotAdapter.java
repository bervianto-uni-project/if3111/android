package org.debez.hmlite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Silva on 2/25/2017.
 */

public class PlotAdapter extends ArrayAdapter<Plot> {
    public PlotAdapter(Context context, ArrayList<Plot> arrPlot) {
        super(context, 0, arrPlot);
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        // Get the data item for this position
        Plot plot = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.plot_layout, parent, false);
        }
        ImageView image = (ImageView) convertView.findViewById(R.id.image);
        TextView plantName = (TextView) convertView.findViewById(R.id.plant_name);
        if(plot.getPlantId() != -1) {
            // Lookup view for data population

            // Populate the data into the template view using the data object
            if (plot.getPlantPhase() < 2) {
                image.setImageResource(R.drawable.plant_muda);
            } else if (plot.getPlantId() == 2) {
                image.setImageResource(R.drawable.plant_remaja);
            } else {
                image.setImageResource(R.drawable.plant_tua);
            }
            plantName.setText(plot.getCreatureName());
        }else{
            plantName.setText("Soil");
            image.setImageResource(R.drawable.soil);
        }
        return convertView;
    }
}