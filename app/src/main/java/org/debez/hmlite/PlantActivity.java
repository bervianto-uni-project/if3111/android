package org.debez.hmlite;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import org.debez.hmlite.services.RestConnectService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Bervianto Leo P on 25/02/2017.
 */

public class PlantActivity extends AppCompatActivity {

    private RestConnectService con;
    private String url = "https://harvestmoonlite.herokuapp.com/getAllPlantDetailedInfo/";
    private GridView gridView;
    private SwipeRefreshLayout swipe;

    public static final String EXTRA_MESSAGE = "org.hmlite.PlantActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant);
        gridView = findViewById(R.id.Plant_Grid);
        swipe = findViewById(R.id.plant_swipe_refresh);

        if (CharacterFragment.char_id != 0) {
            url += CharacterFragment.char_id;
            new getData().execute(url);
        }

        swipe.setOnRefreshListener(
                () -> {
                    // Log.i(LOG_TAG, "onRefresh called from SwipeRefreshLayout");

                    // This method performs the actual data-refresh operation.
                    // The method calls setRefreshing(false) when it's finished.
                    update();
                }
        );
    }

    private void update() {
        if (CharacterFragment.char_id != 0) {
            new getData().execute(url);
        }
    }

    private class getData extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            //Toast.makeText(PlantActivity.this, getString(R.string.loading_data_message), Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(String... url) {
            try {
                con = new RestConnectService(new URL(url[0]),"GET");
                con.startReceive();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result){
            super.onPreExecute();
            try {
                ArrayList<Plot> arrPlot = new ArrayList<Plot>();
                if(con.getResult() != null) {
                    JSONArray plantArr = new JSONArray(con.getResult());
                    if(plantArr.length() > 0) {
                        for (int i = 0; i < plantArr.length(); i++) {
                            JSONObject Plant = plantArr.getJSONObject(i);
                            Plot plot = new Plot(Plant.getInt("plant_id"), Plant.getInt("plant_phase"),
                                    Plant.getInt("plot_id"), Plant.getString("creature_name"), Plant.getInt("plant_creature_id"));
                            arrPlot.add(plot);
                        }
                        //Toast.makeText(PlantActivity.this, getString(R.string.success_get_data), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(PlantActivity.this, getString(R.string.no_connection), Toast.LENGTH_LONG).show();
                }
                final PlotAdapter adapter = new PlotAdapter(PlantActivity.this, arrPlot);
                gridView.setAdapter(adapter);
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        int selected = adapter.getItem(position).getPlantId();
                        if (selected != -1) {
                            Intent intent = new Intent(getApplicationContext(), PlantDetails.class);
                            intent.putExtra(EXTRA_MESSAGE, selected);
                            startActivity(intent);
                        }
                    }
                });
                swipe.setRefreshing(false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}